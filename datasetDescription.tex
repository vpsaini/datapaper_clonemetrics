\section{Dataset Description}
\label{sec:datasetDescription}

The dataset used in this study consists of 12,481 Java projects hosted by Maven~\cite{maven}. 
We provide a comprehensive list of projects with their version information with the dataset. The projects are of varied size and span across various domains including but not limited to information retrieval projects, database projects, server projects, distributed systems, machine learning and natural language processing libraries.

\subsection{Data Source}
We downloaded the entire Maven repository available at UCI Source Code Data Sets ~\cite{Lopes+Bajracharya+Ossher+Baldi:2010}. We then selected the top versions of all the projects which have their source files with them. This gave us 18,852 projects. We removed all those projects that have less than 100 Java statements; this was done to remove very small or empty projects. The filtration reduced the project count to 12,872. For the methods in each of these projects, we separately computed clone detection and software quality metrics. We then merged the results of clone detection and quality metrics, resulting into the total of 12481 projects. This merge step reduced our dataset by 391 projects, for which the merge step generated empty file. 
 
\subsection{Tools}

\subsubsection{Clone Detector: SourcererCC}

We use SourcererCC, a scalable token-based clone detection tool~\cite{sajnani2016sourcerercc}. Sourcerer-CC is capable of detecting Type 1, Type 2, and Type 3 clones. The tool uses a heuristic-optimized index of tokens to significantly reduce the number of code-block comparisons needed to detect the clones, as well as the number of required token comparisons needed to judge a potential clone. SourcererCC has been claimed to scale to a large repository with 250M lines of code in a single workstation (3Ghz i7 CPU, 12Gb RAM). 

We used following configuration to run SourcererCC: a) similarity threshold $=$ 70\%, meaning the methods which share 70\% or more tokens were reported as clones by the tool; and b) Minimum tokens = $25$, meaning the methods with less than $25$ tokens were ignored. This was done to ignore empty method, stubs, method contracts defined in interfaces and abstract classes. SourcererCC has claimed to have a precision of 86\% and recall of 90\% at this chosen configuration~\cite{sajnani2016sourcerercc}.

\subsubsection{JHawk}

We used JHawk~\cite{urlJHawk}, version 6, to calculate method level metrics. JHawk has been 
widely used in academic studies on Java metrics ~\cite{gupta2005fuzzy,alghamdi2005oometer,benestad2006assessing,arisholm2006predicting,arisholm2007data,andersson2004object,nasseri2008empirical,mubarak2009does,mubarak2010evolutionary, borstler2011quality,lucredio2012investigation,scandariato2012predicting,herzigclassifying}. 
we computed all of the 27 software quality metrics supported by JHawk. We divide the metrics into 3 groups based on our best discretion: (i) metrics assessing code complexity; and (ii) metrics assessing modularity properties; and (iii) metrics assessing how well the code is documented. Table~\ref{tab:metric-breakdown} shows these metrics.
\begin{table}[!tbp]
	\begin{center}
		\resizebox{8.4cm}{!}{
			\begin{tabular} {l l l}
				\thickhline
				\hlx{v}
				Category & Name & Description \\
				\hlx{vhv}
				\hlx{vhv}
				Code Complexity & COMP & McCabes cyclomatic complexity\\
				& NOA & Number of arguments\\
				& VDEC & Number of variables declared\\
				& VREF & Number of variables referenced\\
				& NOS & Number of statements\\
				& NEXP & Number of expressions\\
				& MDN & Method, Maximum depth of nesting\\
				& HLTH & Halstead length of method\\
				& HVOC & Halstead vocabulary of method\\
				& HVOL & Halstead volume\\
				& HDIF & Halstead difficulty to implement a method\\
				& HEFF & Halstead effor to implement a method\\
				& TDN & Total depth of nesting\\
				& CAST & Number of class casts\\
				& LOOP & Number of loops (for,while)\\
				& NOPR & Total number of operators\\
				& NAND & Total number of operands\\
				& HBUG & Halstead prediction of number of bugs\\
				& NLOC & Number of lines of code\\
				\hlx{vhv}
				Modularity      & CREF & Number of classes referenced\\
				& XMET & External methods called by the method\\
				& LMET & Local methods called by the method\\
				& EXCR & Number of exceptions referenced by the method\\
				& EXCT & Number of exceptions thrown by the method\\
				& MOD & Number of modifiers\\
				\hlx{vhv}
				Documentation & NOC & Number of comments\\
				& NOCL & Number of comment lines\\
				\thickhline
			\end{tabular}
		}
		\caption{Software Quality Metrics}
		\label{tab:metric-breakdown}
	\end{center}
\end{table}
 
\begin{itemize}
	
	\item \textit{COMP} measures the Cyclomatic complexity of a method. It is the standard metric introduced by McCabe in 1976~\cite{mccabe1976complexity}.	
	
	\item \textit{NOA} measures the total number of arguments expected by a method's signature. 
	
	\item \textit{VDEC} is the total number of variables declared in a method.
	
	\item \textit{VREF} counts the total number of variables referenced in a method. If a method has a large number of arguments, it declares a large number of variables, or it references a large number of variables then, it may be a sign that the method is trying to do too much. It also means that the changeability, tendency to change, of the method is high.
	
	\item \textit{NOS} measures the number of Java statements in a method.  A statement forms a complete unit of execution and are usually terminated with a semicolon (;). A statement can be spread over multiple lines; for example one can decide to start the curly brace of an if statement either from the current line or from the next line. 
	
	\item \textit{NLOC} measures the total number of lines of code in a method.
	
	\item \textit{NEXP} measures the total number of expressions in a method. Oracle notes that ``\textit{An expression is a construct made up of variables, operators, and method invocations, which are constructed according to the syntax of the language, that evaluates to a single value}". A higher value of number of expressions per statement may indicate a complex method.
	
	\item \textit{MDN} is the measure of maximum depth of nesting in a method. A higher depth of nesting in a method indicates higher complexity of the method.
	
	\item \textit{NOPR} and \textit{NAND} measure the total number of operators (arithmetical operators, method names) and the total number of operands (variables, numeric and string constants) used in a method respectively. 
	
	\item \textit{HLTH}, Halstead length~\cite{halstead1977elements}, is the most basic of the Halstead metrics, which simply is the sum of \textit{NOPR} and \textit{NAND}.
	
	\item \textit{ HVOC}, Halstead vocabulary is computed as the sum of unique operators and unique operands in a method. It gives a sense of the complexity among the statements $-$ for example a method which repeatedly uses a small number of variables is less complex than the method which uses a large number of different variables. 
	
	\item \textit{HVOL}, Halstead volume, is computed using \textit{HLTH} and \textit{HVOC} and can be seen as a measure of how much information does a reader need to absorb to understand a method. A small number of statements with a high Halstead Volume would suggest that the individual statements are quite complex.
	
	\item \textit{HDIF}, Halstead Difficulty, uses a formula based on the number of unique operators, the number of unique operands, and the total number of operands to assess the complexity of a method. It suggests how difficult the code is to write and maintain.
	
	\item \textit{HEFF}, Halstead effort, attempts to estimate the amount of work that it would take to recode a particular method.
	
	\item \textit{HBUG}, Halstead bugs, attempts to estimate the number of bugs in a method. 
	
	\item \textit{CREF}, number of classes referenced, measures the total number external classes
	referenced by the method.
	
	\item \textit{XMET} measures the total number of external methods called by a method. External methods are the methods that belong to a class other than the class in which the caller method is defined.
	
	\item \textit{LMET} totals the number of local methods called by a method.
	
	\item \textit{EXCR} and \textit{EXCT} measure the total number of exceptions referenced and the total number of exceptions thrown by a method respectively. 
	
	\item \textit{MOD} is the total number of modifiers (public, private, abstract, static etc.) applied to the signature of a method. 
	
	\item \textit{NOCL} counts the total number of comment lines in a method.
	
	\item \textit{NOC} measures the total number of comments in a method. It is different from \textit{NOCL} as \textit{NOC} counts a comment spread over, say 3 lines, as 1. Whereas \textit{NOCL} will count it as 3. 
	
	
\end{itemize}

Many of the above metrics, (\textit{COMP}, \textit{HLTH}, \textit{HVOC}, \textit{HVOL}, \textit{HDIF}, \textit{HEFF}, \textit{NOPR}, \textit{NAND}, and \textit{HBUG}), are the standard software quality metrics suggested by McCabe~\cite{mccabe1976complexity}, and Halstead~\cite{halstead1977elements}. 

The metrics \textit{CREF}, \textit{XMET}, and \textit{LMET} are derived from the object-oriented (OO) metrics that were suggested by Chidamber and Kemerer~\cite{Chidamber:1994qe}. Basili et al. found that OO metrics appeared to be useful for predicting defect density~\cite{Basili:1994fu}. Subramanyam and Krishnan conducted a survey on eight more empirical studies and showed that OO metrics are significantly associated with defects~\cite{Subramanyam:2003}. 

Additionally, some of these metrics  (\textit{NOS}, \textit{MDN}, \textit{TDN}, \textit{CREF}, \textit{XMET}, \textit{LMET}, \textit{HLTH}, \textit{NOC}, \textit{NOCL}, \textit{CREF}, \textit{XMET}, \textit{LMET}, \textit{NEXP}, \textit{NLOC}) are also part of the SQO-OSS quality model. 
 
\subsection{Data Format}

The dataset consists of the following artifacts.
\begin{itemize}
\item \textit{Source files} for 12,481 Java projects. 
\item \textit{Project list}: We provide a text file which enlists the project names along with their version number, which are present in the dataset. the project name and its version number are separated with $@$ symbol.
\item \textit{Clone-pairs}: method level intra-project clone detection results for the above 12,481 projects. The results for each project are presented in a separate text file. Each row in the text file represents a clone-pair: $<$method1-id,method2-id$>$
\item \textit{Method-id to method-code map}: For each project we provide a separate text file which contains method-id followed by the method-body for each method. Listing~\ref{lst:bookkeepingexample} shows an example. Note that we also provide fully qualified method name separated with $\#$ from the method-id of methods.
\item \textit{Metric values}: for every project we provide a separate comma separated file (CSV), where each row contains computed metrics values for a method found in the given project. Moreover, the rows also contains information, column name \textit{hasClone}, whether we found a clone of this method or not.
\end{itemize}

\subsection{Challenges and Limitations}
In this section we explain a few limitations and challenges of using this dataset.

\begin{lstlisting}[language=Java, label={lst:bookkeepingexample},caption={An example file showing method-id to method-body mapping},frame=tlrb,
basicstyle=\scriptsize, %or \small or \footnotesize etc.
columns=flexible,breaklines=true,breakatwhitespace=true,numbersep=5pt,
xleftmargin=.5cm,
xrightmargin=.5cm,
numberstyle=\footnotesize
]
254742# com.opensymphony.xwork.ObjectFactory.getContinuationPackage

public static String getContinuationPackage() {
return continuationPackage;
}

254744# com.opensymphony.xwork.ObjectFactory.setObjectFactory

public static void setObjectFactory(ObjectFactory factory) {
FACTORY = factory;
}
.
.
.
254888# com.opensymphony.xwork.ObjectFactory.getObjectFactory

public static ObjectFactory getObjectFactory() {
return FACTORY;
}
\end{lstlisting}

\begin{itemize} 
\item The method-ids associated with the methods are unique in a project. These ids are not unique across the entire dataset. This implies that there could be more than one methods for a given method-id in the entire dataset. However, there will be only one method for a given method-id in a project. \item JHawk reports the metric values of a method along with the Project name, package name, class name, and the method name of the method. We used this information to calculate a fully computed method name (FQMN) for every method. Please note that two overloaded methods will have the same FQMN. Despite the limitation of FQMN, we used FQMN to merge the clone detector results with JHawk results; there was no other unique id that could uniquely identify a method in both results.
\end{itemize}